# frozen_string_literal: true

# /etc/udev/rules.d/70-headsets.rules
# # SteelSeries ARCTIS 9
# KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="1038", ATTRS{idProduct}=="12c4", GROUP="plugdev", TAG+="uaccess"
# KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="1038", ATTRS{idProduct}=="12c2", GROUP="plugdev", TAG+="uaccess"
# sudo chmod 666 /dev/bus/usb/001/010
require 'bundler/setup'
require 'libusb'

STDOUT.sync = true
STDERR.sync = true

def generate_data(size, bytes)
  data = bytes.map(&:chr)
  truncate_zeros = size - data.size
  if truncate_zeros
    data.concat (0...truncate_zeros).map { "\x00" }
  end
  data.join
end

id_vendor = 0x1038
# id_product = 0x12c4
id_product =  0x12c2
usb = LIBUSB::Context.new
usb.debug = 10

device = usb.devices(idVendor: id_vendor, idProduct: id_product).first
raise 'Device not found' if device.nil?

endpoint = device.endpoints.detect { |e| e.direction == :in && e.transfer_type == :interrupt }
raise 'Interrupt IN endpoint does not found' if endpoint.nil?

puts "endpoint interrupt in bEndpointAddress => 0x#{endpoint.bEndpointAddress.to_s(16)}"

device.open do |handle|
  handle.auto_detach_kernel_driver = true
  # handle.set_configuration(1) #rescue nil
  # handle.clear_halt(endpoint)
  handle.claim_interface(0) do
    data1 = generate_data 64, [0x10]
    res1 = handle.control_transfer(
      bmRequestType: 0x21,
      bRequest: 0x09,
      wValue: 0x0200,
      wIndex: 0x0000,
      dataOut: data1
    )
    puts "control_transfer => #{res1.inspect}"
    res2 = handle.interrupt_transfer(endpoint: endpoint, dataIn: 100, timeout: 10_000)
    puts "interrupt_transfer => #{res2.inspect}"
  end
end
