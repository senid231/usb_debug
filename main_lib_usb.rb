# frozen_string_literal: true

# http://libusb.sourceforge.net/api-1.0/index.html
# /etc/udev/rules.d/70-headsets.rules
# # SteelSeries ARCTIS 9
# KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="1038", ATTRS{idProduct}=="12c4", GROUP="plugdev", TAG+="uaccess"
# KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="1038", ATTRS{idProduct}=="12c2", GROUP="plugdev", TAG+="uaccess"
# sudo chmod 666 /dev/bus/usb/001/010
require 'bundler/setup'
require_relative 'lib/lib_usb'

STDOUT.sync = true
STDERR.sync = true

def log_result(cmd, result)
  return puts "#{cmd} => #{result.inspect}" unless result.is_a?(Symbol)

  error_desc = LibUsb::Binding.libusb_strerror(result)
  puts "#{cmd} => #{result.inspect} (#{error_desc})"
end

def log_buffer(msg, buffer)
  buffer_string = buffer.get_array_of_uchar(0, buffer.length).map { |i| '%02X' % i }.join(' ').inspect
  puts "#{msg}: #{buffer_string}"
end

id_vendor = 0x1038
# id_product = 0x12c4
id_product = 0x12c2

ctx_ptr = ::FFI::MemoryPointer.new(:pointer, 1)
res_init = LibUsb::Binding.libusb_init(ctx_ptr)
log_result('libusb_init', res_init)
ctx = ctx_ptr.read_pointer
puts "ctx = #{ctx.inspect}"
raise ArgumentError, "can't init libusb" if ctx.null?

# res_debug = LibUsb::Binding.libusb_set_option(ctx, :LOG_LEVEL, :DEBUG)
# log_result('libusb_set_option', res_debug)

dev_handle = LibUsb::Binding.libusb_open_device_with_vid_pid(ctx, id_vendor, id_product)
log_result('libusb_open_device_with_vid_pid', dev_handle)
raise ArgumentError, "can't open device" if dev_handle.null?

begin
  res_detach = LibUsb::Binding.libusb_set_auto_detach_kernel_driver(dev_handle, 1)
  log_result('libusb_set_auto_detach_kernel_driver', res_detach)

  begin
    res_claim = LibUsb::Binding.libusb_claim_interface(dev_handle, 0)
    log_result('libusb_claim_interface', res_claim)

    data = [0x10]
    buffer = FFI::Buffer.new(1, 64, true)
    buffer.put_array_of_uchar(0, data)
    log_buffer('buffer', buffer)
    res_ct = LibUsb::Binding.libusb_control_transfer(
      dev_handle,
      0x21, # bmRequestType
      0x09, # bRequest
      0x0200, # wValue
      0x0000, # wIndex
      buffer, # data
      buffer.length, # wLength
      10_000 # timeout in milliseconds
    )
    log_result('libusb_control_transfer', res_ct)

    buff_i = FFI::Buffer.new(1, 64, true)
    transferred_ptr = FFI::MemoryPointer.new(:int, 1, true)
    res_i = LibUsb::Binding.libusb_interrupt_transfer(
      dev_handle,
      0x81, # endpoint
      buff_i, # data
      buff_i.length, # length
      transferred_ptr, # transferred
      10_000 # timeout
    )
    log_result('libusb_interrupt_transfer', res_i)
    log_buffer('buff_i', buff_i)
  ensure
    res_release = LibUsb::Binding.libusb_release_interface(dev_handle, 0)
    log_result('libusb_release_interface', res_release)
  end

ensure
  LibUsb::Binding.libusb_close(dev_handle)
end
