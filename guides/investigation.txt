https://github.com/Sapd/HeadsetControl/wiki/Development

requests from pc to headset:
usb.bInterfaceClass == HID && usb.endpoint_address.direction != IN && usb.src == host && usb.setup.wLength == 64

whole traffic between pc and headset:
usb.bInterfaceClass == HID && usb.bus_id == 1 && usb.device_address == 7

Sidetone
===============
USB URB
    [Source: host]
    [Destination: 1.7.0]
    USBPcap pseudoheader length: 28
    IRP ID: 0xffff8e89944ed010
    IRP USBD_STATUS: USBD_STATUS_SUCCESS (0x00000000)
    URB Function: URB_FUNCTION_CLASS_INTERFACE (0x001b)
    IRP information: 0x00, Direction: FDO -> PDO
    URB bus id: 1
    Device address: 7
    Endpoint: 0x00, Direction: OUT
    URB transfer type: URB_CONTROL (0x02)
    Packet Data Length: 72
    [Response in: 11937]
    Control transfer stage: Setup (0)
    [bInterfaceClass: HID (0x03)]
Setup Data
    bmRequestType: 0x21
    bRequest: SET_REPORT (0x09)
    wValue: 0x0200
    wIndex: 0
    wLength: 64
    bRequest: 9
    wValue: 0x0200
    wIndex: 0 (0x0000)
    wLength: 64
    Data Fragment: 0600ee000000000000000000000000000000000000000000000000000000000000000000�

USB URB
1c001080aa94898effff000000001b00000100070000024800000000

Setup Data
2109000200004000

Data Fragment (64 bytes)
0600c0 off
0600ee low
0600f8 medium
0600fd high



Save state
==========
Frame 25443: 100 bytes on wire (800 bits), 100 bytes captured (800 bits) on interface wireshark_extcap2304, id 0
USB URB
    [Source: host]
    [Destination: 1.7.0]
    USBPcap pseudoheader length: 28
    IRP ID: 0xffff8e8994507720
    IRP USBD_STATUS: USBD_STATUS_SUCCESS (0x00000000)
    URB Function: URB_FUNCTION_CLASS_INTERFACE (0x001b)
    IRP information: 0x00, Direction: FDO -> PDO
    URB bus id: 1
    Device address: 7
    Endpoint: 0x00, Direction: OUT
    URB transfer type: URB_CONTROL (0x02)
    Packet Data Length: 72
    [Response in: 25444]
    Control transfer stage: Setup (0)
    [bInterfaceClass: HID (0x03)]
Setup Data
    bmRequestType: 0x21
    bRequest: SET_REPORT (0x09)
    wValue: 0x0200
    wIndex: 0
    wLength: 64
    bRequest: 9
    wValue: 0x0200
    wIndex: 0 (0x0000)
    wLength: 64
    Data Fragment: 090000000000000000000000000000000000000000000000000000000000000000000000�

USB URB
1c0020775094898effff000000001b00000100070000024800000000

Setup Data
2109000200004000

Data Fragment
090000






=== open device window
Data Fragment
200000
200000













===== inactive timeout
Frame 59081: 100 bytes on wire (800 bits), 100 bytes captured (800 bits) on interface wireshark_extcap2304, id 0
USB URB
    [Source: host]
    [Destination: 1.7.0]
    USBPcap pseudoheader length: 28
    IRP ID: 0xffff8e8997581060
    IRP USBD_STATUS: USBD_STATUS_SUCCESS (0x00000000)
    URB Function: URB_FUNCTION_CLASS_INTERFACE (0x001b)
    IRP information: 0x00, Direction: FDO -> PDO
    URB bus id: 1
    Device address: 7
    Endpoint: 0x00, Direction: OUT
    URB transfer type: URB_CONTROL (0x02)
    Packet Data Length: 72
    [Response in: 59082]
    Control transfer stage: Setup (0)
    [bInterfaceClass: HID (0x03)]
Setup Data
    bmRequestType: 0x21
    bRequest: SET_REPORT (0x09)
    wValue: 0x0200
    wIndex: 0
    wLength: 64
    bRequest: 9
    wValue: 0x0200
    wIndex: 0 (0x0000)
    wLength: 64
    Data Fragment: 0400012c0000000000000000000000000000000000000000000000000000000000000000�

USB URB
1c0060105897898effff000000001b00000100070000024800000000

Setup Data
2109000200004000

Data Fragment
0400012c 5min
04000258 10min
04000708 30min
04000e10 60min
04001518 90min
04000000 never