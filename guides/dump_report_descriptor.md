https://github.com/DIGImend/usbhid-dump

https://github.com/DIGImend/hidrd

```
$ lsusb | grep SteelSeries
Bus 001 Device 023: ID 1038:12c2 SteelSeries ApS SteelSeries Arctis 9
Bus 001 Device 024: ID 1038:12c4 SteelSeries ApS SteelSeries Arctis 9
```

for 1038:12c4
```
$ sudo usbhid-dump -m1038:12c4 -e all
001:024:005:DESCRIPTOR         1609758530.681373
 05 0C 09 01 A1 01 85 01 15 00 26 FF 00 09 00 75
 08 95 3D 91 02 09 00 81 02 C0

Starting dumping interrupt transfer stream
with 1 minute timeout.

001:024:005:Interrupt transfer timed out
No more interfaces to dump

$ sudo usbhid-dump -m1038:12c4 -i5
001:024:005:DESCRIPTOR         1609758625.614499
 05 0C 09 01 A1 01 85 01 15 00 26 FF 00 09 00 75
 08 95 3D 91 02 09 00 81 02 C0

$ sudo usbhid-dump -m1038:12c4 -i5 | grep -v : | xxd -r -p | hidrd-convert -o spec
Usage Page (Consumer),      ; Consumer (0Ch)
Usage (Consumer Control),   ; Consumer control (01h, application collection)
Collection (Application),
    Report ID (1),
    Logical Minimum (0),
    Logical Maximum (255),
    Usage (00h),
    Report Size (8),
    Report Count (61),
    Output (Variable),
    Usage (00h),
    Input (Variable),
End Collection

$ sudo usbhid-dump -m1038:12c4 -i5 | grep -v : | xxd -r -p | hidrd-convert -o code
0x05, 0x0C,         /*  Usage Page (Consumer),      */
0x09, 0x01,         /*  Usage (Consumer Control),   */
0xA1, 0x01,         /*  Collection (Application),   */
0x85, 0x01,         /*      Report ID (1),          */
0x15, 0x00,         /*      Logical Minimum (0),    */
0x26, 0xFF, 0x00,   /*      Logical Maximum (255),  */
0x09, 0x00,         /*      Usage (00h),            */
0x75, 0x08,         /*      Report Size (8),        */
0x95, 0x3D,         /*      Report Count (61),      */
0x91, 0x02,         /*      Output (Variable),      */
0x09, 0x00,         /*      Usage (00h),            */
0x81, 0x02,         /*      Input (Variable),       */
0xC0                /*  End Collection              */
```

61 output report fields 8 bit each


for 1038:12c2
```
$ sudo usbhid-dump -m1038:12c2 --entity=all
001:023:002:DESCRIPTOR         1609695912.715231
06 00 FF 09 01 A1 01 06 01 FF 15 00 26 FF 00 75
08 09 F0 95 20 81 02 09 F1 95 20 91 02 C0

001:023:001:DESCRIPTOR         1609695912.715425
05 0C 09 01 A1 01 05 0C 19 00 2A FF 0F 15 00 26
FF 0F 75 10 95 02 81 00 C0

001:023:000:DESCRIPTOR         1609695912.715575
06 C0 FF 09 01 A1 01 06 C1 FF 15 00 26 FF 00 75
08 09 F0 95 40 81 02 09 F1 95 40 91 02 09 F2 96
02 02 B1 02 C0

Starting dumping interrupt transfer stream
with 1 minute timeout.

001:023:002:STREAM             1609695912.717068
00 01 00 00 00 00 00 00 09 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00

001:023:001:Interrupt transfer timed out
001:023:000:Interrupt transfer timed out
001:023:002:Interrupt transfer timed out
No more interfaces to dump

$ sudo usbhid-dump -m1038:12c2 -i0 | grep -v : | xxd -r -p | hidrd-convert -o spec
Usage Page (FFC0h),         ; FFC0h, vendor-defined
Usage (01h),
Collection (Application),
Usage Page (FFC1h),     ; FFC1h, vendor-defined
Logical Minimum (0),
Logical Maximum (255),
Report Size (8),
Usage (F0h),
Report Count (64),
Input (Variable),
Usage (F1h),
Report Count (64),
Output (Variable),
Usage (F2h),
Report Count (514),
Feature (Variable),
End Collection

$ sudo usbhid-dump -m1038:12c2 -i1 | grep -v : | xxd -r -p | hidrd-convert -o spec
Usage Page (Consumer),      ; Consumer (0Ch)
Usage (Consumer Control),   ; Consumer control (01h, application collection)
Collection (Application),
Usage Page (Consumer),  ; Consumer (0Ch)
Usage Minimum (00h),
Usage Maximum (0FFFh),
Logical Minimum (0),
Logical Maximum (4095),
Report Size (16),
Report Count (2),
Input,
End Collection

$ sudo usbhid-dump -m1038:12c2 -i2 | grep -v : | xxd -r -p | hidrd-convert -o spec
Usage Page (FF00h),         ; FF00h, vendor-defined
Usage (01h),
Collection (Application),
Usage Page (FF01h),     ; FF01h, vendor-defined
Logical Minimum (0),
Logical Maximum (255),
Report Size (8),
Usage (F0h),
Report Count (32),
Input (Variable),
Usage (F1h),
Report Count (32),
Output (Variable),
End Collection

$ sudo usbhid-dump -m1038:12c2 -i0 | grep -v : | xxd -r -p | hidrd-convert -o code
0x06, 0xC0, 0xFF,   /*  Usage Page (FFC0h),         */
0x09, 0x01,         /*  Usage (01h),                */
0xA1, 0x01,         /*  Collection (Application),   */
0x06, 0xC1, 0xFF,   /*      Usage Page (FFC1h),     */
0x15, 0x00,         /*      Logical Minimum (0),    */
0x26, 0xFF, 0x00,   /*      Logical Maximum (255),  */
0x75, 0x08,         /*      Report Size (8),        */
0x09, 0xF0,         /*      Usage (F0h),            */
0x95, 0x40,         /*      Report Count (64),      */
0x81, 0x02,         /*      Input (Variable),       */
0x09, 0xF1,         /*      Usage (F1h),            */
0x95, 0x40,         /*      Report Count (64),      */
0x91, 0x02,         /*      Output (Variable),      */
0x09, 0xF2,         /*      Usage (F2h),            */
0x96, 0x02, 0x02,   /*      Report Count (514),     */
0xB1, 0x02,         /*      Feature (Variable),     */
0xC0                /*  End Collection              */

$ sudo usbhid-dump -m1038:12c2 -i1 | grep -v : | xxd -r -p | hidrd-convert -o code
0x05, 0x0C,         /*  Usage Page (Consumer),      */
0x09, 0x01,         /*  Usage (Consumer Control),   */
0xA1, 0x01,         /*  Collection (Application),   */
0x05, 0x0C,         /*      Usage Page (Consumer),  */
0x19, 0x00,         /*      Usage Minimum (00h),    */
0x2A, 0xFF, 0x0F,   /*      Usage Maximum (0FFFh),  */
0x15, 0x00,         /*      Logical Minimum (0),    */
0x26, 0xFF, 0x0F,   /*      Logical Maximum (4095), */
0x75, 0x10,         /*      Report Size (16),       */
0x95, 0x02,         /*      Report Count (2),       */
0x81, 0x00,         /*      Input,                  */
0xC0                /*  End Collection              */

$ sudo usbhid-dump -m1038:12c2 -i2 | grep -v : | xxd -r -p | hidrd-convert -o code
0x06, 0x00, 0xFF,   /*  Usage Page (FF00h),         */
0x09, 0x01,         /*  Usage (01h),                */
0xA1, 0x01,         /*  Collection (Application),   */
0x06, 0x01, 0xFF,   /*      Usage Page (FF01h),     */
0x15, 0x00,         /*      Logical Minimum (0),    */
0x26, 0xFF, 0x00,   /*      Logical Maximum (255),  */
0x75, 0x08,         /*      Report Size (8),        */
0x09, 0xF0,         /*      Usage (F0h),            */
0x95, 0x20,         /*      Report Count (32),      */
0x81, 0x02,         /*      Input (Variable),       */
0x09, 0xF1,         /*      Usage (F1h),            */
0x95, 0x20,         /*      Report Count (32),      */
0x91, 0x02,         /*      Output (Variable),      */
0xC0                /*  End Collection              */
```
