# frozen_string_literal: true

# $ cat /etc/udev/rules.d/99-steelseries.rules
# ACTION!="add|change", GOTO="steelseries_end"
#
# # SteelSeries Arctis 9
# KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="1038", ATTRS{idProduct}=="12c4", GROUP="plugdev", TAG+="uaccess"
# KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="1038", ATTRS{idProduct}=="12c2", GROUP="plugdev", TAG+="uaccess"
#
# LABEL="steelseries_end"
# $ sudo udevadm control --reload-rules && sudo udevadm trigger
require 'bundler/setup'
require_relative 'lib/hidapi'

STDOUT.sync = true
STDERR.sync = true
HIDAPI::Configuration.logger.level = Logger::DEBUG

HIDAPI.init_library do
  HIDAPI::Device.find(0x1038, 0x12c2) do |device|
    bytes = device.read(64, timeout: 2_000)
    puts "read #{HIDAPI::Util.ascii_bytes(bytes)}"

    device.write [0x10], count: 64

    bytes = device.read(64, timeout: 2_000)
    puts "read #{HIDAPI::Util.ascii_bytes(bytes)}"

    device.write [0x0d], count: 64

    # device.write [0x06, 0x00, 0xee], count: 64
    # device.write [0x03], count: 64
    # device.write [0x2b], count: 64
    # device.write [0x07], count: 64
    # device.write [0x2c], count: 64
    # device.write [0x0b], count: 64
    # device.write [0x05], count: 64
    # device.write [0x04, 0x00, 0x07, 0x08], count: 64
    # bytes = device.read(64, timeout: 2_000)
    # puts "read #{HIDAPI::Util.ascii_bytes(bytes)}"
    # device.write [0x09], count: 64

    device.write [0x20], count: 64
    bytes = device.read(64, timeout: 2_000)
    puts "read #{HIDAPI::Util.ascii_bytes(bytes)}"

    device.write [0x0d], count: 64 # this command on exit
  end
end
