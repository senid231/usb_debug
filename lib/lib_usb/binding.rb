# frozen_string_literal: true

require 'ffi'

module LibUsb
  module Binding
    extend FFI::Library

    ffi_lib ['libusb-1.0.so.0', 'libusb-1.0', 'libusb']

    typedef :pointer, :libusb_device_handle
    typedef :pointer, :libusb_context

    enum :libusb_option, [
      :LOG_LEVEL, 0,
      :USE_USBDK, 1,
      :WEAK_AUTHORITY, 2
    ]

    enum :libusb_log_level, [
      :NONE, 0, # No messages ever emitted by the library (default)
      :ERROR, 1, # Error messages are emitted
      :WARNING, 2, # Warning and error messages are emitted
      :INFO, 3, # Informational, warning and error messages are emitted
      :DEBUG, 4 # All messages are emitted
    ]

    enum :libusb_error, [
      :SUCCESS, 0, # Success (no error)
      :ERROR_IO, -1, # Input/output error.
      :ERROR_INVALID_PARAM, -2, # Invalid parameter.
      :ERROR_ACCESS, -3, # Access denied (insufficient permissions)
      :ERROR_NO_DEVICE, -4, # No such device (it may have been disconnected)
      :ERROR_NOT_FOUND, -5, # Entity not found.
      :ERROR_BUSY, -6, # Resource busy.
      :ERROR_TIMEOUT, -7, # Operation timed out.
      :ERROR_OVERFLOW, -8, # Overflow.
      :ERROR_PIPE, -9, # Pipe error.
      :ERROR_INTERRUPTED, -10, # System call interrupted (perhaps due to signal)
      :ERROR_NO_MEM, -11, # Insufficient memory.
      :ERROR_NOT_SUPPORTED, -12, # Operation not supported or unimplemented on this platform.
      :ERROR_OTHER, -99 # Other error.
    ]

    # int libusb_init	(
    #   libusb_context ** 	context
    # )
    attach_function :libusb_init, [
      :pointer
    ], :libusb_error

    # int libusb_set_option	(
    #   libusb_context * 	ctx,
    #   enum libusb_option 	option,
    #  	...
    # )
    attach_function :libusb_set_option, [
      :libusb_context,
      :libusb_option,
      :libusb_log_level
    ], :void

    # void libusb_exit	(
    #   libusb_context * 	ctx
    # )
    attach_function :libusb_exit, [
      :libusb_context
    ], :void

    # const char* libusb_error_name	(
    #   int 	error_code
    # )
    attach_function :libusb_error_name, [
      :libusb_error
    ], :string

    # const char* libusb_strerror	(
    #   int 	errcode
    # )
    attach_function :libusb_strerror, [
      :libusb_error
    ], :string


    # libusb_device_handle* libusb_open_device_with_vid_pid	(
    #   libusb_context * 	ctx,
    #   uint16_t 	vendor_id,
    #   uint16_t 	product_id
    # )
    attach_function :libusb_open_device_with_vid_pid, [
      :libusb_context,
      :uint16,
      :uint16
    ], :libusb_device_handle

    # void libusb_close	(
    #   libusb_device_handle * 	dev_handle
    # )
    attach_function :libusb_close, [
      :libusb_device_handle
    ], :void

    # int libusb_set_auto_detach_kernel_driver	(	libusb_device_handle * 	dev_handle,
    # int 	enable
    # )
    attach_function :libusb_set_auto_detach_kernel_driver, [
      :libusb_device_handle,
      :int
    ], :libusb_error

    # int libusb_claim_interface	(	libusb_device_handle * 	dev_handle,
    # int 	interface_number
    # )
    attach_function :libusb_claim_interface, [
      :libusb_device_handle,
      :int
    ], :libusb_error

    # int libusb_release_interface	(	libusb_device_handle * 	dev_handle,
    # int 	interface_number
    # )
    attach_function :libusb_release_interface, [
      :libusb_device_handle,
      :int
    ], :libusb_error

    # int libusb_control_transfer	(
    #   libusb_device_handle * 	dev_handle,
    #   uint8_t 	bmRequestType,
    #   uint8_t 	bRequest,
    #   uint16_t 	wValue,
    #   uint16_t 	wIndex,
    #   unsigned char * 	data,
    #   uint16_t 	wLength,
    #   unsigned int 	timeout
    # )
    attach_function :libusb_control_transfer, [
      :libusb_device_handle,
      :uint8,
      :uint8,
      :uint16,
      :uint16,
      :buffer_in,
      :uint16,
      :uint
    ], :libusb_error

    # int 	libusb_bulk_transfer (
    #   libusb_device_handle *dev_handle,
    #   unsigned char endpoint,
    #   unsigned char *data,
    #   int length,
    #   int *transferred,
    #   unsigned int timeout
    # )
    attach_function :libusb_bulk_transfer, [
      :libusb_device_handle,
      :uchar,
      :buffer_in,
      :int,
      :pointer,
      :uint
    ], :libusb_error

    # int 	libusb_interrupt_transfer (
    #   libusb_device_handle *dev_handle,
    #   unsigned char endpoint,
    #   unsigned char *data,
    #   int length,
    #   int *transferred,
    #   unsigned int timeout
    # )
    attach_function :libusb_interrupt_transfer, [
      :libusb_device_handle,
      :uchar,
      :buffer_out,
      :int,
      :pointer,
      :uint
    ], :libusb_error

    # int libusb_set_configuration	(
    #   libusb_device_handle * 	dev_handle,
    #   int 	configuration
    # )
    attach_function :libusb_set_configuration, [
      :libusb_device_handle,
      :int
    ], :libusb_error

    # int libusb_get_configuration	(
    #   libusb_device_handle * 	dev_handle,
    #   int * 	config
    # )
    attach_function :libusb_get_configuration, [
      :libusb_device_handle,
      :pointer
    ], :libusb_error
  end
end
