# frozen_string_literal: true

require_relative 'binding/wide_string'
require_relative 'binding/device_info'

module HIDAPI
  # https://github.com/libusb/hidapi/blob/master/hidapi/hidapi.h
  # inspired by
  # https://github.com/gareth/ruby_hid_api
  module Binding
    extend FFI::Library

    ffi_lib Configuration.library

    typedef Binding::DeviceInfo.auto_ptr, :device_info
    typedef Binding::WideString, :wide_string
    typedef :pointer, :device
    typedef :int, :vendor_id
    typedef :int, :product_id
    typedef :int, :serial_number
    typedef :int, :length
    typedef :int, :timeout

    attach_function :hid_init, [], :int
    attach_function :hid_exit, [], :int
    attach_function :hid_enumerate, [:vendor_id, :product_id], :device_info
    attach_function :hid_free_enumeration, [:device_info], :void
    attach_function :hid_open, [:vendor_id, :product_id, :serial_number], :device
    attach_function :hid_open_path, [:string], :device
    attach_function :hid_write, [:device, :buffer_in, :length], :int
    attach_function :hid_read_timeout, [:device, :buffer_out, :length, :timeout], :int
    attach_function :hid_read, [:device, :buffer_out, :length], :int
    attach_function :hid_set_nonblocking, [:device, :int], :int
    attach_function :hid_send_feature_report, [:device, :buffer_in, :length], :int
    attach_function :hid_get_feature_report, [:device, :buffer_out, :length], :int
    attach_function :hid_get_input_report, [:device, :buffer_out, :length], :int
    attach_function :hid_close, [:device], :void
    attach_function :hid_get_manufacturer_string, [:device, :buffer_out, :length], :int
    attach_function :hid_get_product_string, [:device, :buffer_out, :length], :int
    attach_function :hid_get_serial_number_string, [:device, :buffer_out, :length], :int
    attach_function :hid_get_indexed_string, [:device, :int, :buffer_out, :length], :int
    # hid_error will always return a nil except on Windows
    attach_function :hid_error, [:device], Binding::WideString
  end
end
