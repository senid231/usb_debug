module HIDAPI
  module Binding
    class DeviceInfo < FFI::Struct
      include Enumerable

      def self.release(pointer)
        HIDAPI.hid_free_enumeration(pointer) unless pointer.null?
      end

      # Struct layout from http://www.signal11.us/oss/hidapi/hidapi/doxygen/html/structhid__device__info.html
      layout :path, :string, # char * path
             :vendor_id, :ushort, # unsigned short vendor_id
             :product_id, :ushort, # unsigned short product_id
             :serial_number, WideString, # wchar_t * serial_number
             :release_number, :ushort, # unsigned short release_number
             :manufacturer_string, WideString, # wchar_t * manufacturer_string
             :product_string, WideString, # wchar_t * product_string
             :usage_page, :ushort, # unsigned short usage_page
             :usage, :ushort, # unsigned short usage
             :interface_number, :int, # int interface_number
             :next, DeviceInfo.auto_ptr # struct hid_device_info * next

      # Makes the struct members available as methods
      members.each do |name|
        define_method(name) { self[name] }
      end

      def inspect
        product_string.tap do |s|
          s << format(" (%s)", path) unless path.empty?
        end
      end

      # Exposes the linked list structure in an Enumerable-compatible format
      def each
        return enum_for(:each) unless block_given?

        pointer = self
        loop do
          break if pointer.null?
          yield pointer
          pointer = pointer.next
        end
      end
    end
  end
end
