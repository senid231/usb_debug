# frozen_string_literal: true

module HIDAPI
  module Binding
    class WideString
      extend FFI::DataConverter
      native_type FFI::Type::POINTER

      class << self
        def from_native(value, _context)
          return nil if value.null?
          HIDAPI::Util.read_wchar_string(value)
        end
      end
    end
  end
end
