# frozen_string_literal: true

module HIDAPI
  module Util
    WCHAR_T_WIDTH = ::RUBY_PLATFORM =~ /mswin/i ? 2 : 4

    # Maps a WCHAR_T_WIDTH to the corresponding Array#pack character width
    WCHAR_T_FORMATS = {
      2 => "S",
      4 => "L"
    }.freeze

    # The platform-specific Array#pack format for wide characters
    WCHAR_T_FORMAT = WCHAR_T_FORMATS[WCHAR_T_WIDTH].freeze

    # Maps a WCHAR_T_WIDTH to the corresponding string encoding
    WCHAR_T_ENCODINGS = {
      2 => "utf-16le",
      4 => "utf-32le"
    }.freeze

    # The platform-specific String encoding that can handle wide characters
    WCHAR_T_ENCODING = WCHAR_T_ENCODINGS[WCHAR_T_WIDTH].freeze

    module_function

    def read_wchar_string(pointer)
      buffer = []
      offset = 0
      loop do
        current_pointer = pointer + (offset * WCHAR_T_WIDTH)
        char = current_pointer.send("read_uint#{WCHAR_T_WIDTH * 8}")
        break if char.zero?
        buffer << char
        offset += 1
      end
      buffer.pack("#{WCHAR_T_FORMAT}*").force_encoding(WCHAR_T_ENCODING).encode("utf-8")
    end

    def build_buffer(data, count: nil, item_size: 1, clear: true)
      count ||= data.count
      buffer = FFI::Buffer.new(item_size, count, clear)
      buffer.put_array_of_uchar(0, data) unless data.nil?
      buffer
    end

    def buffer_to_bytes(buffer)
      buffer.get_array_of_uchar(0, buffer.length)
    end

    def human_readable_buffer(buffer, format: '%02X', cut_tail_zeros: true)
      bytes = buffer_to_bytes(buffer)
      human_readable_bytes(bytes, format: format, cut_tail_zeros: cut_tail_zeros)
    end

    # @param bytes [Array<Integer>,nil]
    # @return [String]
    def human_readable_bytes(bytes, format: '%02X', cut_tail_zeros: true)
      return '(0)' if bytes.nil? || bytes.empty?

      tail = -1
      bytes.reverse.each { |byte| byte.zero? ? tail -= 1 : break } if cut_tail_zeros
      formatted_bytes = bytes[0..tail].map { |byte| sprintf(format, byte) }
      "(#{bytes.size}) #{formatted_bytes.join(' ')}"
    end

    # @param bytes [Array<Integer>]
    # @return [String]
    def ascii_bytes(bytes, not_readable: '·')
      return '' if bytes.nil? || bytes.empty?

      bytes.map { |byte| byte >= 32 && byte <= 126 ? byte.chr : not_readable }.join
    end
  end
end
