# frozen_string_literal: true

require 'singleton'
require 'forwardable'

module HIDAPI
  class Configuration
    include Singleton
    extend SingleForwardable

    class << self
      # @param name [Symbol,String] method name
      # @param default [Object] default value
      def define_config(name, default: nil)
        setter = :"#{name}="
        attr_accessor(name)
        def_single_delegators(:instance, name, setter)
        instance.public_send(setter, default)
      end

      def define_config_method(name, &block)
        define_method(name, &block)
        def_single_delegators(:instance, name)
      end
    end

    # @!method library [String,Array<String>]
    # @!method library=(String,Array<String>)
    define_config :library, default: ['hidapi', 'hidapi-hidraw', 'hidapi-libusb'].freeze

    # @!method logger [Logger,nil]
    # @!method logger=(Logger,nil)
    define_config :logger, default: Logger.new(STDOUT)
    logger.level = Logger::INFO
  end
end
