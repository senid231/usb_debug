# frozen_string_literal: true

module HIDAPI
  class Device
    class << self
      # @param vendor_id [Integer,String]
      # @param product_id [Integer,String]
      # @param serial_number [Integer,String,nil]
      # @raise [HIDAPI::Error]
      # @yield [HIDAPI::Device]
      # @return [HIDAPI::Device,nil]
      def find(vendor_id, product_id, serial_number = nil)
        serial_number ||= 0
        vendor_id = "0x#{vendor_id}".to_i(16) if vendor_id.is_a?(String)
        product_id = "0x#{product_id}".to_i(16) if product_id.is_a?(String)
        serial_number = "0x#{serial_number}".to_i(16) if serial_number.is_a?(String)

        HIDAPI.init_library
        ptr = Binding.hid_open(vendor_id, product_id, serial_number)
        raise Error, 'device not found' if ptr.null?

        device = new(ptr)
        return device unless block_given?

        begin
          yield(device)
        ensure
          device.close
        end
        nil
      end
    end

    # @param pointer [FFI::Pointer]
    def initialize(pointer)
      raise Error, 'invalid pointer' if pointer.nil? || pointer.null?

      @ptr = pointer
      ObjectSpace.define_finalizer(self, proc { close })
    end

    def to_ptr
      @ptr
    end

    # @param bytes [Array<Integer>]
    # @param count [Integer,nil]
    # @raise [HIDAPI::ClosedError] if device already closed
    # @raise [HIDAPI::CommandError] on failed result
    def write(bytes, count: nil)
      check_closed
      buffer_in = Util.build_buffer(bytes, count: count)
      result = Binding.hid_write(to_ptr, buffer_in, buffer_in.length)
      log_buffer_in('hid_write', buffer_in)
      handle_result('hid_write', result)
    end

    # @param bytes [Array<Integer>]
    # @param count [Integer,nil]
    # @raise [HIDAPI::ClosedError] if device already closed
    # @raise [HIDAPI::CommandError] on failed result
    def send_feature_report(bytes, count: nil)
      check_closed
      buffer_in = Util.build_buffer(bytes, count: count)
      result = Binding.hid_send_feature_report(to_ptr, buffer_in, buffer_in.length)
      log_buffer_in('hid_send_feature_report', buffer_in)
      handle_result('hid_send_feature_report', result)
    end

    # @param count [Integer]
    # @param timeout [Integer,nil] how many milliseconds wait for result (default 10 000)
    # @raise [HIDAPI::ClosedError] if device already closed
    # @raise [HIDAPI::CommandError] on failed result
    # @return [Array<Integer>,nil]
    def read(count, timeout: 10_000)
      check_closed
      buffer_out = Util.build_buffer(nil, count: count)
      result = Binding.hid_read_timeout(to_ptr, buffer_out, buffer_out.length, timeout)
      log_buffer_out('hid_read_timeout', buffer_out)
      handle_result('hid_read_timeout', result)
      Util.buffer_to_bytes(buffer_out) unless result.zero?
    end

    # @param count [Integer]
    # @raise [HIDAPI::ClosedError] if device already closed
    # @raise [HIDAPI::CommandError] on failed result
    # @return [Array<Integer>,nil]
    def get_feature_report(count)
      check_closed
      buffer_out = Util.build_buffer(nil, count: count)
      result = Binding.hid_get_feature_report(to_ptr, buffer_out, buffer_out.length)
      log_buffer_out('hid_get_feature_report', buffer_out)
      handle_result('hid_get_feature_report', result)
      Util.buffer_to_bytes(buffer_out) unless result.zero?
    end

    # @param count [Integer]
    # @raise [HIDAPI::ClosedError] if device already closed
    # @raise [HIDAPI::CommandError] on failed result
    # @return [Array<Integer>,nil]
    def get_input_report(count)
      check_closed
      buffer_out = Util.build_buffer(nil, count: count)
      result = Binding.get_input_report(to_ptr, buffer_out, buffer_out.length)
      log_buffer_out('get_input_report', buffer_out)
      handle_result('get_input_report', result)
      Util.buffer_to_bytes(buffer_out) unless result.zero?
    end

    # @raise [HIDAPI::ClosedError] if device already closed
    # @raise [HIDAPI::CommandError] on failed result
    # @return [String]
    def get_manufacturer
      check_closed
      buffer_out = Util.build_buffer(nil, count: 255, item_size: :wchar_t)
      result = Binding.hid_get_manufacturer_string(to_ptr, buffer_out, buffer_out.length)
      log_buffer_out('hid_get_manufacturer_string', buffer_out)
      handle_result('hid_get_manufacturer_string', result)
      Util.read_wchar_string(buffer_out)
    end

    # @raise [HIDAPI::ClosedError] if device already closed
    # @raise [HIDAPI::CommandError] on failed result
    # @return [String]
    def get_product
      check_closed
      buffer_out = Util.build_buffer(nil, count: 255, item_size: :wchar_t)
      result = Binding.hid_get_product_string(to_ptr, buffer_out, buffer_out.length)
      log_buffer_out('hid_get_product_string', buffer_out)
      handle_result('hid_get_product_string', result)
      Util.read_wchar_string(buffer_out)
    end

    # @raise [HIDAPI::ClosedError] if device already closed
    # @raise [HIDAPI::CommandError] on failed result
    # @return [String]
    def get_serial_number
      check_closed
      buffer_out = Util.build_buffer(nil, count: 255, item_size: :wchar_t)
      result = Binding.hid_get_serial_number_string(to_ptr, buffer_out, buffer_out.length)
      log_buffer_out('hid_get_serial_number_string', buffer_out)
      handle_result('hid_get_serial_number_string', result)
      Util.read_wchar_string(buffer_out)
    end

    def get_indexed_string(index)
      check_closed
      buffer_out = Util.build_buffer(nil, count: 255, item_size: :wchar_t)
      result = Binding.hid_get_indexed_string(dev_handle, index, buffer_out, buffer_out.length)
      log_buffer_out('get_indexed_string', buffer_out)
      return unless handle_result('get_indexed_string', result, safe: true)

      Util.read_wchar_string(buffer_out)
    end

    def close
      return false if to_ptr.null?

      Binding.hid_close(to_ptr)
      @ptr = FFI::Pointer.new(0)
      true
    end

    private

    # @param command [String]
    # @param result [Integer]
    # @param safe [Boolean] when true method returns false value instead of raise error (default false)
    # @raise [HIDAPI::CommandError] if result negative
    # @return [Boolean]
    def handle_result(command, result, safe: false)
      logger&.debug { "#{command} => #{result.inspect}" }
      raise CommandError.new(command, to_ptr) if !safe && result.negative?

      !result.negative?
    end

    # @raise [HIDAPI::ClosedError] if device already closed
    def check_closed
      raise ClosedError, 'device is closed' if to_ptr.null?
    end

    # @param command [String]
    # @param buffer [FFI::Buffer]
    def log_buffer_in(command, buffer)
      formatted_bytes = Util.human_readable_buffer(buffer)
      logger&.debug { "#{command} called with #{formatted_bytes}" }
    end

    # @param command [String]
    # @param buffer [FFI::Buffer]
    def log_buffer_out(command, buffer)
      formatted_bytes = Util.human_readable_buffer(buffer)
      logger&.debug { "#{command} responds with #{formatted_bytes}" }
    end

    def logger
      Configuration.logger
    end
  end
end
