# frozen_string_literal: true

module HIDAPI
  class Error < StandardError
  end

  class ClosedError < Error
  end

  class CommandError < Error
    def initialize(command, device_pointer)
      error_description = HIDAPI::Binding.hid_error(device_pointer)
      super("#{command} was failed with #{error_description}")
    end
  end
end
