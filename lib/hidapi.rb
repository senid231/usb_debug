# frozen_string_literal: true

require 'ffi'
require 'active_support/all'
require_relative 'hidapi/configuration'
require_relative 'hidapi/util'
require_relative 'hidapi/binding'
require_relative 'hidapi/error'
require_relative 'hidapi/device'

module HIDAPI
  module_function

  mattr_accessor :_init_called

  def init_library
    if block_given?
      begin
        init_library
        yield
      ensure
        exit_library
      end
      return
    end

    return false if _init_called

    result = Binding.hid_init
    Configuration.logger&.debug { "hid_init => #{result.inspect}" }
    raise Error, 'init failed' if result.negative?

    self._init_called = true
  end

  def exit_library
    return false unless _init_called

    Binding.hid_exit
    Configuration.logger&.debug { 'hid_exit =>' }
    self._init_called = false
    true
  end
end
